from django.shortcuts import render, redirect
# Create your views here.
from .models import AdminDetails
from .forms import AdminForm
from django.contrib import messages
from .admin import Admin

def Insert_view(request):
    form = AdminForm
    if request.method == "POST":
        form = AdminForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Data Inserted Successfully")
            return redirect("/")
    # print(form)
    return render(request, "insert.html", {"form": form})


def show(request):
    admins = AdminDetails.objects.all()
    return render(request, 'show.html', {"admins": admins})


def delete(request, id):
    admins = AdminDetails.objects.get(id=id)
    admins.delete()
    messages.success(request,"Data Deleted Successfully")
    return redirect("/show")


def update(request, id):
    admins = AdminDetails.objects.get(id=id)
    form = AdminForm(request.POST,instance=admins)
    if form.is_valid():
        form.save(commit=True)
        messages.success(request, "Data Updated Successfully")
        return redirect("/show")

    return render(request,'update.html',{'admins':admins})