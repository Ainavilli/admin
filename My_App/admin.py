from django.contrib import admin

from My_App.models import AdminDetails

class Admin(admin.ModelAdmin):
    fields=['Grades','Sections','Subjects']


admin.site.register(AdminDetails,Admin)