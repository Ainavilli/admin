from .models import AdminDetails
from django import forms
from .admin import Admin


class AdminForm(forms.ModelForm):
    class Meta:
        model = AdminDetails
        fields = "__all__"