from django.db import models

# Create your models here.

class AdminDetails(models.Model):
    Grades= models.IntegerField()
    Sections = models.CharField(max_length=100)
    Subjects = models.CharField(max_length=100)
